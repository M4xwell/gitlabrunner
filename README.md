# Setup Runner

1. Get your Runner Registration Token from ` Admin Area > Runners `
2. Edit the `REGISTRATION_TOKEN` variable in the [gitlab-runner script](./gitlab-runner.sh)

