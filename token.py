#!/usr/bin/python

import psycopg2

HOST='172.18.0.3'
DATABASE='gitlabhq_production'
USER='gitlab'
PASSWORD='gitlab'

con = psycopg2.connect(host=HOST, database=DATABASE, user=USER, password=PASSWORD)   
cur = con.cursor()
cur.execute("SELECT runners_registration_token FROM application_settings ORDER BY id DESC LIMIT 1")
con.commit()
con.close()